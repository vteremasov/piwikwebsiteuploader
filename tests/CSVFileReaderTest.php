<?php

use piwikwebsiteuploader\CSVFileReader;

class CSVFileReaderTest extends \PHPUnit_Framework_TestCase {
    /** @var  CSVFileReader */
    private $csvFileReader;

    public function setUp(){
        $this->csvFileReader = new CSVFileReader();
    }


    public function testSingleLine()
    {
        $this->readCsvAndAssert(<<<END
name, url
'asdf', 'http://asdf.com'
END
, array(
                array('name' => 'asdf', 'url' => 'http://asdf.com'),
       )
        );
    }

    public function testTwoLines()
    {
        $this->readCsvAndAssert(<<<END
name, url
'asdf', 'http://asdf.com'
'qwerty', 'http://qwerty.com'
END
            , array(
                array('name' => 'asdf', 'url' => 'http://asdf.com'),
                array('name' => 'qwerty', 'url' => 'http://qwerty.com'),
            )
        );
    }

    public function testMultiLines()
    {
        $this->readCsvAndAssert(<<<END
name, url
'asdf', 'http://asdf.com'
'qwerty', 'http://qwerty.com'
'qwerty', 'http://qwerty.com'
'qwerty', 'http://qwerty.com'
END
            , array(
                array('name' => 'asdf', 'url' => 'http://asdf.com'),
                array('name' => 'qwerty', 'url' => 'http://qwerty.com'),
                array('name' => 'qwerty', 'url' => 'http://qwerty.com'),
                array('name' => 'qwerty', 'url' => 'http://qwerty.com'),
            )
        );
    }

  /**
     * @param $CONTENT
     * @param $expected
     */
    public function readCsvAndAssert($CONTENT, $expected)
    {
        $filename = tempnam(sys_get_temp_dir(), __CLASS__);
        file_put_contents($filename, $CONTENT);
        $this->csvFileReader->setCSVFile($filename);
        $this->csvFileReader->setMaxLineLength(1000);
        $this->csvFileReader->setDelimiter(",");
        $this->csvFileReader->setEnclosure("'");
        $this->csvFileReader->setEscape(" ");

      //WTF? remove all csv text, except header lines then run tests, it would be GREEN!!!
      //TODO rewrite loop with expected data!!!
        $index = 0;
        foreach ($this->csvFileReader as $line) {
            $this->assertEquals($expected[$index], $line);
            $index++;
        }
    }
}
 