<?php
use piwikwebsiteuploader\PiwikSiteLoader;


class PiwikSiteLoaderTest extends PHPUnit_Framework_TestCase
{

    private $loader;
    private $csvReader;
    private $piwikAdapter;

    public function setUp()
    {
        $this->loader = new PiwikSiteLoader();
        $this->csvReader = $this->getMock('\piwikwebsiteuploader\ICSVReader');
        $this->piwikAdapter = $this->getMock('\piwikwebsiteuploader\IPiwikAdapter');
        $this->loader->setCSVReader($this->csvReader);
        $this->loader->setPiwikAdapter($this->piwikAdapter);
    }

    public function testEmptyFile()
    {
        $this->csvReader->expects($this->once())->method('rewind');
        $this->csvReader->expects($this->once())->method('valid')->will($this->returnValue(false));
        $this->loader->load();
    }

    public function testSingleLineFile()
    {
        $line = "asdf";
        $this->csvReader->expects($this->at(0))->method('rewind');
        $this->csvReader->expects($this->at(1))->method('valid')->will($this->returnValue(true));
        $this->csvReader->expects($this->at(2))->method('current')->will($this->returnValue($line));
        $this->csvReader->expects($this->at(3))->method('next');
        $this->csvReader->expects($this->at(4))->method('valid')->will($this->returnValue(false));
        $this->piwikAdapter->expects($this->once())->method('addSite')->with($line);
        $this->loader->load();
    }
}
 