<?php

use piwikwebsiteuploader\ErrorRecorder;
use piwikwebsiteuploader\LoadSiteException;

class ErrorRecorderTest extends PHPUnit_Framework_TestCase {

    private $expactedErrors = array();

    public function testSingleError(){
        $errorRecorder = new ErrorRecorder();
        $error = new LoadSiteException("My error");
        $errorRecorder->recordErrors($error);
        $result = array();
        foreach($errorRecorder->getErrors() as $error){array_push($result, $error->getMessage());}
        $this->assertEquals(array("My error"), $result);
    }

    public function testSeveralErrors(){
        $errorRecorder = new ErrorRecorder();
        $index = 0;
        while ($index < 6) {
            $error = new LoadSiteException("My error");
            $errorRecorder->recordErrors($error);
            $index++;
        }
        $result = array();
        foreach($errorRecorder->getErrors() as $error){array_push($result, $error->getMessage());}
        $this->assertEquals(array("My error", "My error",
            "My error","My error",
            "My error","My error"), $result);
    }

    public function testSeveralErrorsGetTrace(){
        $errorRecorder = new ErrorRecorder();
        $index = 0;        while ($index < 6) {
            $error = new LoadSiteException("My error");
            $trueError = new Exception("My error");
            $this->prepareResult($trueError);
            $errorRecorder->recordErrors($error);
            $index++;
        }
        $result = $this->expactedErrors;
        /** @var $arrayErs \Exception */
        $arrayErs = $errorRecorder->getErrors();
        for($i=0; $i<count($result); $i++)
        {
            $this->assertEquals($result[$i]->getTrace(), $arrayErs[$i]->getTrace());
        }
    }

    private function prepareResult($trueError)
    {
        array_push($this->expactedErrors, $trueError);
    }

}
 