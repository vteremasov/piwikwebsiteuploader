<?php

use piwikwebsiteuploader\LoadSiteException;
use piwikwebsiteuploader\PiwikAdapter;

class PiwikAdapterTests extends PHPUnit_Framework_TestCase
{

    public function test_SitesManager_addSite_with_correct_data()
    {
        list($piwikAdapter, $expected, $correctSiteData) = $this->setUp();

        $this->assertNotEquals($expected, $piwikAdapter->addSite($correctSiteData));
    }

    public function test_SitesManager_addSite_with_invalid_data_will_throw_an_Exception()
    {
        list($piwikAdapter, $expected, $correctSiteData, $invalidSiteData) = $this->setUp();
        try{
        $this->assertEquals($expected, $piwikAdapter->addSite($invalidSiteData));
        }catch (LoadSiteException $e){
            return true;
        }
        throw new Exception("Test not passed");
    }

    /**
     * @return array
     */
    public function setUp()
    {
        $piwikAdapter = new PiwikAdapter();
        $url = 'http://localhost/';
        $piwikAdapter->setUrl($url);
        //WTF? i can't see which data from which source, too many magic strings
        $expected = "method=SitesManager.addSite" .
            " &siteName=TestName" .
            " &urls=test.url.org" .
            " &timezone=Africa/Douala" .
            " &currency=EUR &keepURLFragments='0'" .
            " &token_auth=e0dd669bdfe0821b8083fe92b0689426 ";
        $correctSiteData = array("&siteName" => "TestName",
            "&urls" => "test.url.org",
            "&timezone" => "Africa/Douala",
            "&currency" => "EUR",
            "&keepURLFragments" => "'0'",
            "&token_auth" => "e0dd669bdfe0821b8083fe92b0689426");

        $invalidSiteData = array("&siteName" => "TestName",
            "&urls" => "test.url.org",
            "&timezone" => "Africa/Douala",
            "&currency" => "EURasdf",
            "&keepURLFragments" => "'0'",
            "&token_auth" => "e0dd669bdfe0821b8083fe92b0689426");

        return array($piwikAdapter, $expected, $correctSiteData, $invalidSiteData);
    }
}
 