<?php
require_once 'vendor/autoload.php';

use piwikwebsiteuploader\CSVFileReader;
use piwikwebsiteuploader\ErrorHandler;
use piwikwebsiteuploader\ErrorRecorder;
use piwikwebsiteuploader\PiwikSiteLoader;
use piwikwebsiteuploader\PiwikAdapter;

$errors = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $errorHandler = new ErrorHandler();
    $errorsRecorder = new ErrorRecorder();

    $errorHandler->setErrorRecorder($errorsRecorder);

    $reader = new CSVFileReader();
    $loader = new PiwikSiteLoader();
    $piwikAdapter = new PiwikAdapter();

    $loader->setErrorHandler($errorHandler);
    $reader->setCSVFile($_FILES['csv_file']['tmp_name']);
    $reader->setMaxLineLength(1000);
    $reader->setDelimiter(",");
    $reader->setEnclosure("'");
    $reader->setEscape(" ");

    $piwikAdapter->setUrl($_POST['url']);
    $loader->setCSVReader($reader);
    $loader->setPiwikAdapter($piwikAdapter);
    $loader->load();
    $errors = $errorsRecorder->getErrors();
}
?>
<!DOCTYPE html>
<html>
<body>

<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data">
    <label>Select a csv file: </label>
    <input type="file" name="csv_file">
    </br>

    <label>Specify piwik url:
        <input type="text" name="url">
    </label>

    </br>
    <button type="submit">Submit</button>
</form>

<?php if ($_SERVER['REQUEST_METHOD'] == 'POST'): ?>
    <h1>Upload Result:</h1>
    <? $errorsQuantity = 0; ?>
    <ul>
        <?php foreach ($errors as $error): ?>
            <li><?php echo $error->getMessage(); ?></li>
            <? $errorsQuantity++; ?>
        <?php endforeach; ?>
        <li>You have <? echo $loader->uploadsQuantity-$errorsQuantity; ?> success uploads</li>
        <li>You have <? echo $errorsQuantity; ?> errors</li>
    </ul>
<?php endif; ?>
</body>
</html>
