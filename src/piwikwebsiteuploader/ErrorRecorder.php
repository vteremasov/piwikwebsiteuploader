<?php

namespace piwikwebsiteuploader;


class ErrorRecorder {
    private $errors = array();

    /**
     * @param $error \Exception
     */
    public function recordErrors($error)
    {
        array_push($this->errors, $error);
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

}