<?php
namespace piwikwebsiteuploader;

class CSVFileReader implements ICSVReader
{
    private $csvFile;
    private $fileHandle;
    private $lastLineRead;
    private $currentSiteData;
    private $headerLine;
    private $_index = 0;

    private $maxLineLength;

    private $delimiter;

    private $enclosure;

    private $escape;

    public function setCSVFile($csvFile)
    {
        $this->csvFile = realpath($csvFile);
    }

    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
    }

    public function setEnclosure($enclosure)
    {
        $this->enclosure = $enclosure;
    }

    public function setEscape($escape)
    {
        $this->escape = $escape;
    }

    public function setMaxLineLength($maxLineLength)
    {
        $this->maxLineLength = $maxLineLength;
    }


    public function current()
    {
        return $this->currentSiteData[$this->_index];
    }

    public function next()
    {
        $this->_index++;
    }

    public function key()
    {
        return $this->_index;
    }

    public function valid()
    {
        if ($this->fileHandle === false || $this->fileHandle === null) return false;
        $this->lastLineRead = fgetcsv($this->fileHandle, $this->maxLineLength,
            $this->delimiter, $this->enclosure, $this->escape);
        if ($this->lastLineRead === false) return false;
        if (array(null) !== $this->lastLineRead) {
            $this->currentSiteData[$this->_index] = $this->trimLine($this->lastLineRead);
        }
        return isset($this->currentSiteData);
    }

    public function rewind()
    {
        if (!is_null($this->fileHandle)) {
            $this->stopReading();
        }
        if (is_null($this->fileHandle)) {
            $this->startReading();
        }
    }

    private function trimLine($line)
    {
        $result = array();
        $result = $this->trimWordInLine($line, $result);
        return $result;
    }

    public function trimArray($headerLine)
    {
        $_index = 0;
        foreach ($headerLine as $line) {
            $headerLine[$_index] = trim($line);
            $_index++;
        }
        return $headerLine;
    }

    private function stopReading()
    {
        fclose($this->fileHandle);
        $this->fileHandle = null;
        $this->_index = 0;
    }

    private function startReading()
    {
        if (!file_exists($this->csvFile)) {
            throw new LoadSiteException("File does not exist.");
        } else {
            $this->fileHandle = fopen($this->csvFile, "r");
            if (!is_null($this->fileHandle)) {
                $this->headerLine = fgetcsv($this->fileHandle, $this->maxLineLength,
                    $this->delimiter, $this->enclosure, $this->escape);
                $this->headerLine = $this->trimArray($this->headerLine);
            }
        }
    }

    private function trimWordInLine($line, $result)
    {
        for ($i = 0; $i < count($this->headerLine); $i++) {
            $result[$this->headerLine[$i]] = isset($line[$i]) ? trim($line[$i]) : null;
        }
        return $result;
    }
}