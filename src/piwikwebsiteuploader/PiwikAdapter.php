<?php

namespace piwikwebsiteuploader;


class PiwikAdapter implements IPiwikAdapter
{

    private $url;
    private $errorMessage = array();

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function addSite($siteData)
    {
        $request = $this->prepareRequestString($siteData);
        $result = file_get_contents($request);
        if ($this->isError($result))
            throw new LoadSiteException($this->errorMessage);

    }

    /**
     * @param $singleSiteData
     * @return string
     */
    private function prepareRequestString($singleSiteData)
    {
        $this->url .= "?module=API&method=SitesManager.addSite";
      //WTF? Does $value it urlencoded?
      //WTF? Does $key it urlencoded?
      //WTF? where is separator '&'? it's unclear.
        foreach ($singleSiteData as $key => $value) $this->url .= $key . "=" . $value;
        return $this->url;
    }

    private function isError($result)
    {
      //WTF? what if $result not a XML document?
        $result = new \SimpleXMLElement($result);
        if (array_key_exists("error", $result)) {
            $this->errorMessage = array_map(function($arr){return $arr["message"];}, (array)$result);
            $this->errorMessage = (string)$this->errorMessage["error"];
            return true;
        }
        else return false;
    }
}
