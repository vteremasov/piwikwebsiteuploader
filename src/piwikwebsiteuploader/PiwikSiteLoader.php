<?php
namespace piwikwebsiteuploader;


class PiwikSiteLoader
{
    public $uploadsQuantity = 0;
    /** @var  ICSVReader */
    private $csvReader;
    /** @var  IPiwikAdapter */
    private $piwikAdapter;
    /** @var  ErrorHandler */
    private $errorHandler;

    /**
     * @param $piwikAdapter IPiwikAdapter
     */    public function setPiwikAdapter(IPiwikAdapter $piwikAdapter)
    {
        $this->piwikAdapter = $piwikAdapter;
    }

    /**
     * @param $csvReader ICSVReader
     */
    public function setCSVReader($csvReader)
    {
        $this->csvReader = $csvReader;
    }

    /**
     * @param $errorHandler ErrorHandler
     */
    public function setErrorHandler($errorHandler)
    {
        $this->errorHandler = $errorHandler;
    }


    public function load()
    {
        foreach($this->csvReader as $line){
            $this->uploadsQuantity++;
            try {
                $this->piwikAdapter->addSite($line);
            } catch (LoadSiteException $e) {

                $this->errorHandler->handleException($e);
            }
        }
    }
}