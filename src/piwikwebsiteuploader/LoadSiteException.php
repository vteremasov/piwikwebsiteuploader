<?php

namespace piwikwebsiteuploader;


class LoadSiteException extends \Exception{


    function __construct($result)
    {
        parent::__construct($result);
    }
}