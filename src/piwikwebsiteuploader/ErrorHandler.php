<?php

namespace piwikwebsiteuploader;


class ErrorHandler
{
    /** @var  ErrorRecorder*/
    private $errorRecorder;

    /**
     * @param $errorRecorder ErrorRecorder
     */
    public function setErrorRecorder($errorRecorder)
    {
        $this->errorRecorder = $errorRecorder;
    }

    /**
     * @param $e \Exception
     */
    public function handleException($e)
    {
        $this->errorRecorder->recordErrors($e);
    }
}
